import requests


class TagGenerator:
    API_KEY = 'AIzaSyD28KsefEXQn-arorNxDi5GnYo9nU_w4R8'

    def find_tags(self, imgUrl):
        print("Finding tags...")

        url = 'https://vision.googleapis.com/v1/images:annotate?key=' + self.API_KEY

        data = '''{
          "requests": [
            {
              "features": [
                {
                  "type": "LABEL_DETECTION"
                }
              ],
              "image": {
                "source": {
                  "imageUri": "PUT_URL_HERE"
                }
              }
            }
          ]
        } '''

        data = data.replace("PUT_URL_HERE", imgUrl)
        response = requests.post(url, data=data)

        response = response.json()
        responses = response['responses']
        print(' ')

        print('Hash tags')
        print('---------')
        print(' ')

        tags_list = []

        for response in responses:
            labels = response['labelAnnotations']
            for label in labels:
                output_str = ""
                output_str += label['description'].replace(' ', '')
                print(output_str)
                tags_list.append(output_str)

        return tags_list
